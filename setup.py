#!/usr/bin/env python3

from setuptools import setup
from setuptools import find_packages


setup(
    name="simplertc",
    description="Simple real time clock with Wishbone interface",
    author="Raptor Engineering, LLC",
    author_email="support@raptorengineering.com",
    url="https://www.raptorengineering.com",
    download_url="https://gitlab.raptorengineering.com/kestrel-collaboration/kestrel-litex/simplertc",
    test_suite="test",
    license="BSD",
    python_requires="~=3.6",
    packages=find_packages(exclude=("test*", "sim*", "doc*", "examples*")),
    include_package_data=True,
)
