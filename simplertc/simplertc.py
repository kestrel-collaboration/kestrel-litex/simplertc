#!/usr/bin/env python3

# This file is Copyright (c) 2018-2019 Florent Kermarrec <florent@enjoy-digital.fr>
# This file is Copyright (c) 2018-2019 David Shah <dave@ds0.me>
# This file is Copyright (c) 2020-2022 Raptor Engineering, LLC
# License: BSD

import os
import argparse
import subprocess
import tempfile

from migen import *

from litex import get_data_mod

from litex.soc.interconnect import wishbone, stream
from litex.soc.interconnect.csr import *
from litex.gen.common import reverse_bytes

kB = 1024
mB = 1024*kB

# SimpleRTC Slave interface ------------------------------------------------------------------------------

from litex.soc.interconnect import wishbone, stream
from litex.gen.common import reverse_bytes

class SimpleRTCSlave(Module, AutoCSR):
    def __init__(self, platform, endianness="big", rtc_clk_src="rtc", rtc_clk_freq=1e6):
        self.slave_bus    = slave_bus    = wishbone.Interface(data_width=32, adr_width=30)

        # Bus endianness handlers
        self.slave_dat_w = Signal(32)
        self.slave_dat_r = Signal(32)
        self.comb += self.slave_dat_w.eq(slave_bus.dat_w if endianness == "big" else reverse_bytes(slave_bus.dat_w))
        self.comb += slave_bus.dat_r.eq(self.slave_dat_r if endianness == "big" else reverse_bytes(self.slave_dat_r))

        self.specials += Instance("simple_rtc_wishbone",
            # Parameters
            p_BASE_CLOCK_FREQUENCY_KHZ = int(rtc_clk_freq / 1000.0),

            # Wishbone slave port signals
            i_slave_wb_cyc = slave_bus.cyc,
            i_slave_wb_stb = slave_bus.stb,
            i_slave_wb_we = slave_bus.we,
            i_slave_wb_addr = slave_bus.adr,
            i_slave_wb_dat_w = self.slave_dat_w,
            o_slave_wb_dat_r = self.slave_dat_r,
            i_slave_wb_sel = slave_bus.sel,
            o_slave_wb_ack = slave_bus.ack,
            o_slave_wb_err = slave_bus.err,

            # Clock and reset
            # Put the peripheral on the both main system clock and reset domains
            i_peripheral_reset = ResetSignal('sys'),
            i_peripheral_clock = ClockSignal('sys'),
            i_rtc_clock = ClockSignal(rtc_clk_src),
        )
        self.add_sources(platform)


    @staticmethod
    def add_sources(platform):
        vdir = get_data_mod("peripheral", "simplertc").data_location
        platform.add_source(os.path.join(vdir, "simple_rtc.v"))
